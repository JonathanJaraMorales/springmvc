package com.jjaram.spring.mvc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jjaram.spring.mvc.entity.User;
import com.jjaram.spring.mvc.repositoty.RUser;
import com.jjaram.spring.mvc.service.interfaces.IUser;

/**
 * 
 * @author Jonathan Jara Morales
 *
 */
@Service ("UserService")
public class UserService implements IUser{
	
	@Autowired
	private RUser repository;
	
	/**
	 * This method simulate a called 
	 */
	
	@RequestMapping(value = "/home2", method = RequestMethod.GET)
	public User findUser(int id) {
		return repository.findUser(id);
	}

}
