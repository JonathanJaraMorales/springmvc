package com.jjaram.spring.mvc.controller;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jjaram.spring.mvc.service.UserService;


@Controller
public class UserController implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Autowired
	private UserService service;
	
	@RequestMapping(value = "/persistence", method = RequestMethod.GET)
	public String getUser(Model model){
		model.addAttribute("toString", service.findUser(1).toString() );
		return null;
	}
	
	
	

}
