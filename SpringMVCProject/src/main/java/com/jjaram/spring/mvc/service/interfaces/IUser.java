package com.jjaram.spring.mvc.service.interfaces;

import org.springframework.stereotype.Repository;
import com.jjaram.spring.mvc.entity.User;

@Repository
public interface IUser{
	public User findUser(int id);
}
